
#  Dharma has never been so easy

## Debtors

What to take out a loan? Check out the `debtor.js` script! On top of the file is a JSON object containing all the parameters that define a loan. If those parameters look good, run it with `node debtor.js`

Want to take out a different loan? Update the values in the JSON object and then run it & you're good to go! Check out ethindia.bohendo.com & see if your debt order shows up. I hope it gets filled!

## Creditors

Want to invest some of your capital? Check out the `creditor.js` script! Run it and you're money will find a safe home in one of the many debt orders on display at ethindia.bohendo.com!

Check out the [Relayer's ethereum address](https://kovan.etherscan.io/address/0x80e0d8d9917eb48cad307be74a4541cc93cd367a) & watch for outgoing transactions that prove you're money is safely invested.

