const fs = require('fs')
const request = require('request')
const ethutils = require('ethereumjs-util')

const url = 'https://ethindia.bohendo.com'

const pk = Buffer.from('1b8a66d258109ede07e2ad6474ab22426fe0b8ec2865978e14b6d34439ab5b14', 'hex')

const debtorAddress = '0x' + ethutils.privateToAddress(pk).toString('hex')

const debtor_request = { 
    principalAmount: 5,
    principalToken: "WETH",
    collateralAmount: 100,
    collateralToken: "REP",
    interestRate: 12.3,
    termDuration: 6,
    termUnit: "months",
    debtorAddress: debtorAddress,
    expiresInDuration: 5,
    expiresInUnit: "days",
}

request.post({
    url: url + '/api/create',
    json: true,
    body: debtor_request,
}, (error, response, body) => {


    const signature = ethutils.ecsign(ethutils.toBuffer(body.loanRequestHash), pk)

    body.debtorSignature = {
        v: ethutils.bufferToHex(signature.v),
        r: ethutils.bufferToHex(signature.r),
        s: ethutils.bufferToHex(signature.s)
    }

    return request.post({
        url: url + '/api/submit',
        json: true,
        body,
    }, (error, response, body) => {
        console.log(`Successfully submited a debt order! Navigate to ethindia.bohendo.com & see if you can find it. Good luck getting it filled!`)
    })

})


