const fs = require('fs')
const request = require('request')
const ethutils = require('ethereumjs-util')

const pk = Buffer.from('a3e5ac7a06b6f4729f3349bd16112bd404cade5082ef1ef589a58155417c27a2', 'hex')
const url = 'https://ethindia.bohendo.com'
const creditorAddress = '0x' + ethutils.privateToAddress(pk).toString('hex')

request.post({
    url: url + '/api/get/',
}, (error, response, body) => {

    const orderToSign = JSON.parse(body)[0]

    console.log('hash:' + body.loanRequestHash)
    const signature = ethutils.ecsign(ethutils.toBuffer(body.loanRequestHash), pk)

    const signed_order = orderToSign
    signed_order.creditorSignature = {}
    signed_order.creditorSignature.v = ethutils.bufferToHex(signature.v)
    signed_order.creditorSignature.r = ethutils.bufferToHex(signature.r)
    signed_order.creditorSignature.s = ethutils.bufferToHex(signature.s)

    return request.post({
        url: url + '/api/fill',
        json: true,
        body: signed_order,
    }, (error, response, body) => {
        console.log(`Successfully filled a debt order! Navigate to ethindia.bohendo.com!`)
    })

})
